module Test.TTT.Game(gameTests) where

import Test.HUnit
import TTT.Game
import Control.Monad

playMoves :: Board -> [Action] -> Maybe Board
playMoves = foldM makeMove

diagSeq = [Action C1 C1, Action C1 C2, Action C2 C2, Action C2 C1, Action C3 C3]

winTest = TestCase (assertEqual "result of playing X on diagonal" (Just $ Win X) (getGameState <$> (playMoves initialState diagSeq)))


gameTests = TestList [TestLabel "winTest 1" winTest]
