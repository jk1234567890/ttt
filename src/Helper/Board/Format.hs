{-# LANGUAGE ScopedTypeVariables #-}
module Helper.Board.Format(drawBoard,parseMove) where

import Control.Monad
import Data.Digits

import Helper.Numbers

pad :: Int -> String -> String
pad ct str = replicate (ct - length str) ' ' ++ str

drawLine :: Int -> [String] -> String
drawLine _ [] = []
drawLine wi [x] = pad wi x
drawLine wi (x:xs) = pad wi x ++ "|" ++ drawLine wi xs

drawBoard :: forall a c r. (Bounded c, Enum c, Bounded r, Enum r) =>
  (a -> c -> r -> String) -> a -> String
drawBoard getField board =
  pad rowLabelWidth "" ++ "|" ++ drawLine colLabelWidth (makeNumber 'a' 'z' <$> [0..width-1])
    ++ "\n" ++ concatMap drawRow [minBound..maxBound]
  where
    width = fromEnum (maxBound::r) - fromEnum (minBound::r) + 1
    height= fromEnum (maxBound::c) - fromEnum (minBound::c) + 1
    colLabelWidth = numericFieldWidth (fromEnum 'z'-fromEnum 'a'+1) width
    rowLabelWidth = numericFieldWidth 10 height
    drawRow :: r -> String
    drawRow num = pad rowLabelWidth $ makeNumber '0' '9' (fromEnum num+1) ++ "|"
      ++ drawLine colLabelWidth (getField board <$> [minBound..maxBound] <*> pure num) ++ "\n"

convertTypeMaybe :: forall a. (Bounded a, Enum a) => Int -> Maybe a
convertTypeMaybe = fmap toEnum . (mfilter (<=fromEnum (maxBound::a))) . Just

parseMove :: forall c r a. (Bounded r, Enum r, Bounded c, Enum c) => (c -> r -> a) -> String -> Maybe a
parseMove coordToAction input = uncurry coordToAction <$> ((verifySplit $ span (`elem`['a'..'z']) input) >>= uncurry parseSplit)
  where
    verifySplit :: (String, String) -> Maybe (String, String)
    verifySplit (l,d)
      | not (null l) && not (null d) && all (`elem`['a'..'z']) l && all (`elem`['0'..'9']) d = Just (l,d)
      | otherwise = Nothing
    parseSplit :: String -> String -> Maybe (c, r)
    parseSplit letters dig = (,) <$>
      convertTypeMaybe (charsToNum 'a' 26 letters) <*>
      convertTypeMaybe (charsToNum '0' 10 dig - 1)
      where
        charsToNum zz base str = unDigits base $ map ((+(-fromEnum zz)).fromEnum) str 
