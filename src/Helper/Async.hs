{-# LANGUAGE RankNTypes #-}
module Helper.Async where

import Data.IORef

import FRP.Grapefruit.Signal
import FRP.Grapefruit.Signal.Discrete as DSignal
import FRP.Grapefruit.Circuit
import FRP.Grapefruit.Setup

import Control.Concurrent
import Control.Monad
import Control.Arrow

data AsyncIOWorker a b = AsyncIOWorker {
  responseHandler :: IORef (Maybe (b -> IO ())),
  syncRunner :: IO () -> IO (),
  calculateFunction :: a -> IO b
}

type AsyncWorkerProvider a b = (a -> IO b) -> AsyncIOWorker a b

createAsyncIOWorker :: (IO () -> IO ()) -> (a -> IO b) -> IO (AsyncIOWorker a b)
createAsyncIOWorker sr fun =
  (\r -> AsyncIOWorker r sr fun ) <$> newIORef Nothing 


prepareAsyncIOWorker :: IO ((IO () -> IO ()) -> AsyncWorkerProvider a b)
prepareAsyncIOWorker = AsyncIOWorker <$> newIORef Nothing

asyncWorkerCircuit :: AsyncIOWorker a b -> forall era. Circuit era (DSignal era a) (DSignal era b)
asyncWorkerCircuit (AsyncIOWorker responseHan sr fun) = consume con >>> produce pr
 where
   pr = producer (\handler ->
                setup $ (writeIORef responseHan (Just handler) >> return (writeIORef responseHan Nothing))
             )
   con = consumer (\input -> void $ forkOS $ (do
                       res <- fun input
                       han <- res `seq` readIORef responseHan
                       maybe (return ()) (\h -> sr $ h res) han
                                              ))
    

