{-# LANGUAGE BangPatterns #-}
module Helper.Numbers where

import Data.Digits
import Data.Sequence as Seq


numericFieldWidth :: Int -> Int -> Int
numericFieldWidth base value = nfwRec value 0
  where
    nfwRec 0 !len = len
    nfwRec !v !len = nfwRec (v `div` base) (len+1)

makeNumber :: Char -> Char -> Int -> String
makeNumber firstDigit _ 0 = [firstDigit]
makeNumber firstDigit lastDigit num = (index dig) <$> digits base num  
  where
    dig = Seq.fromList [firstDigit..lastDigit]
    base = fromEnum lastDigit - fromEnum firstDigit + 1
