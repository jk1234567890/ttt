{-# LANGUAGE RankNTypes #-}
module Helper.Delay(prepareDelayCircuit, delayCircuit, DelaySpace) where

import FRP.Grapefruit.Signal.Continuous as CSignal
import FRP.Grapefruit.Signal.Segmented as SSignal
import FRP.Grapefruit.Circuit
import FRP.Grapefruit.Signal

import Control.Arrow

import Data.IORef

data DelaySpace a = DelaySpace { delayRef :: IORef a }

prepareDelayCircuit :: IO (DelaySpace a)
prepareDelayCircuit = DelaySpace <$> newIORef undefined

delayCircuit :: DelaySpace a -> forall era. Circuit era (SSignal era a) (CSignal era a)
delayCircuit (DelaySpace ref) = consume cons >>> produce prod
  where 
    cons = SSignal.consumer (writeIORef ref)
    prod = CSignal.producer (readIORef ref) 
