{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Game.Interface where

import Data.Sequence
import Control.Monad.Reader.Class

data Game s m p = Game {
  initialState :: s
 ,possibleMoves :: s -> Seq m
 ,makeMove :: s -> m -> s
 ,players :: Seq p
 ,payoffsOrPlayer :: s -> Either (p -> Double) p
}


class GameRulesExtractor g s m p | g -> s m p where
  extractGame :: g -> Game s m p


gameInitialState :: (MonadReader g m, GameRulesExtractor g s move p) => m s
gameInitialState = initialState . extractGame <$> ask
gamePossibleMoves :: (MonadReader g m, GameRulesExtractor g s move p) => m (s -> Seq move)
gamePossibleMoves = possibleMoves . extractGame <$> ask
gameMakeMove :: (MonadReader g m, GameRulesExtractor g s move p) => m (s -> move -> s)
gameMakeMove = makeMove . extractGame <$> ask 
gamePayoffsOrPlayer :: (MonadReader g m, GameRulesExtractor g s move p) => s -> m (Either (p -> Double) p)
gamePayoffsOrPlayer st = (payoffsOrPlayer . extractGame <$> ask) <*> pure st
gamePlayers :: (MonadReader g m, GameRulesExtractor g s move p) => m (Seq p)
gamePlayers = players .extractGame <$> ask


instance GameRulesExtractor (Game s m p) s m p where
  extractGame = id
