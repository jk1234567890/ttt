module TTT.Interface(tttGame) where

import qualified TTT.Game as G
import Game.Interface
import Data.Sequence as Seq

tttPayoffs :: G.Board -> Either (G.Symbol -> Double) G.Symbol
tttPayoffs board = case G.getGameState board of
  G.Win G.X -> Left winX
  G.Win G.O -> Left winO
  G.Draw -> Left $ const 0
  G.Move G.X -> Right G.X
  G.Move G.O -> Right G.O
  where
    winX G.X = 1
    winX G.O = -1
    winO s = -(winX s)

tttGame :: Game G.Board G.Action G.Symbol
tttGame = Game
  G.initialState
  (Seq.fromList . G.possibleMoves)
  G.makeMoveUnsafe
  (Seq.fromList [G.X, G.O])
  tttPayoffs
