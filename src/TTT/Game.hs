{-# LANGUAGE TemplateHaskell, Rank2Types #-}
module TTT.Game(Symbol(..),Row, Board, GameState(..), Coord(..), Action(..)
               ,isEndGame, getGameState, makeMove, initialState, coordSeq
               ,getField, boardToString, opponent, possibleMoves, makeMoveUnsafe) where

import Control.Applicative
import Control.Lens
import Data.Maybe(isJust, fromJust)
import Data.List

data Symbol = X|O deriving (Show, Eq, Read)
data Row = Row {_c1::(Maybe Symbol), _c2::(Maybe Symbol),  _c3::(Maybe Symbol)} deriving Show
data Board = Board {_r1::Row, _r2::Row, _r3::Row} deriving Show

data GameState = Move Symbol | Win Symbol | Draw deriving (Eq, Show, Read)
data Coord = C1 | C2 | C3 deriving (Eq, Show, Read, Ord, Enum, Bounded)
data Action = Action {aX::Coord, aY::Coord} deriving (Eq, Show, Read, Ord)

coordSeq :: [Coord]
coordSeq = [C1, C2, C3]

makeLenses ''Row
makeLenses ''Board

opponent :: Symbol -> Symbol
opponent X = O
opponent O = X

getField :: Board -> Coord -> Coord -> Maybe Symbol
getField board r c= board^.(moveToRow r).(moveToCol c)

firstRow :: Board -> Row
firstRow = _r1

secondRow :: Board -> Row
secondRow = _r2

thirdRow :: Board -> Row
thirdRow = _r3

rowToList :: Row -> [Maybe Symbol]
rowToList (Row a b c) = a:b:c:[]

firstColumn :: Board -> [Maybe Symbol]
firstColumn (Board (Row a _ _) (Row b _ _) (Row c _ _)) = a:b:c:[]

secondColumn :: Board -> [Maybe Symbol]
secondColumn (Board (Row _ a _) (Row _ b _) (Row _ c _)) = a:b:c:[]

thirdColumn :: Board -> [Maybe Symbol]
thirdColumn (Board (Row _ _ a) (Row _ _ b) (Row _ _ c)) = a:b:c:[]

diagonal :: Board -> [Maybe Symbol]
diagonal (Board (Row a _ _) (Row _ b _) (Row _ _ c)) = [a,b,c]

antiDiagonal :: Board -> [Maybe Symbol]
antiDiagonal (Board (Row _ _ a) (Row _ b _) (Row c _ _)) = [a,b,c]

allSymbols :: Board -> [Maybe Symbol]
allSymbols b = (concat $ map (\x -> x b) $ map (rowToList.) [firstRow, secondRow, thirdRow])

isEndGame :: GameState -> Bool
isEndGame (Move _) = False
isEndGame (Win _) = True
isEndGame Draw = True

checkSystem :: [Maybe Symbol] -> Maybe Symbol

checkSystem list
  | is X = Just X
  | is O = Just O
  | otherwise = Nothing
  where
    is symbol = all (== (Just symbol)) list

getWinner :: Board -> Maybe Symbol
getWinner board
  | xx = Just X
  | yy = Just O
  | otherwise = Nothing
  where
    xx = (Just X) `elem` matches
    yy = (Just O) `elem` matches
    matches = ZipList (map (checkSystem.) patterns) <*> pure board
    patterns = [rowToList.firstRow, rowToList.secondRow, rowToList.thirdRow, firstColumn, secondColumn, thirdColumn, diagonal, antiDiagonal]

getGameState :: Board -> GameState
getGameState board
  | winner == (Just X) = Win X
  | winner == (Just O) = Win O
  | xs+os==9 = Draw
  | xs > os = Move O
  | xs == os = Move X
  | otherwise = error "bad game state"
  where
    winner = getWinner board
    sym = allSymbols board
    xs = length $ filter (== (Just X)) sym
    os = length $ filter (== (Just O)) sym


moveToRow :: Coord -> Lens' Board Row
moveToRow C1 = r1
moveToRow C2 = r2
moveToRow C3 = r3

moveToCol :: Coord -> Lens' Row (Maybe Symbol)
moveToCol C1 = c1
moveToCol C2 = c2
moveToCol C3 = c3

makeMove :: Board -> Action -> Maybe Board
makeMove board (Action xx yy) =
  if isJust piece
    then  Nothing
    else  change . Just <$> currentPiece
  where
    change :: Maybe Symbol -> Board
    change ms = set (x.y) ms board 
    x :: Lens' Board Row
    x = moveToRow xx
    y :: Lens' Row (Maybe Symbol)
    y = moveToCol yy
    piece :: Maybe Symbol
    piece = board^.x.y
    currentMove = getGameState board
    currentPiece = case currentMove of
      (Move O) -> Just O
      (Move X) -> Just X
      _ -> Nothing

makeMoveUnsafe :: Board -> Action -> Board
makeMoveUnsafe board action = fromJust $  makeMove board action

initialState :: Board
initialState = Board (Row Nothing Nothing Nothing) (Row Nothing Nothing Nothing) (Row Nothing Nothing Nothing)

boardToString :: Board -> String
boardToString board = mconcat $  intersperse "\n" $ visualizeRow . (board^.) . (moveToRow) <$> coordSeq
  where
    visualizeRow row = mconcat $  intersperse "|" (visualizeCell . (row^.) . (moveToCol) <$> coordSeq)
    visualizeCell Nothing = " "
    visualizeCell (Just X) = "X"
    visualizeCell (Just O) = "O"


possibleMoves :: Board -> [Action]
possibleMoves board = filter checkEmpty allMoves
  where
    allMoves = uncurry Action <$> ((,) <$> coordSeq <*> coordSeq)
    coordToGetter :: Action -> Getter Board (Maybe Symbol)
    coordToGetter (Action xc yc) = (moveToRow xc) . (moveToCol yc)
    checkEmpty action = (board^.(coordToGetter action)) == Nothing
