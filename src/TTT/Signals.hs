{-# LANGUAGE Arrows, TemplateHaskell #-}
module TTT.Signals(GameError(..), GameDescription, wireSignals, board, state, lastError, ssMsg, errMsg, statusMessage, GameAction(..)) where

import FRP.Grapefruit.Circuit
import TTT.Game
import FRP.Grapefruit.Signal.Discrete as DSignal
import FRP.Grapefruit.Signal.Segmented as SSignal
import Control.Arrow
import Control.Lens

data GameAction = M Action | Restart

data GameError = IllegalMove

data GameDescription = GameDescription {
  _board :: Board,
  _state :: GameState,
  _lastError :: Maybe GameError
                                       }

makeLenses ''GameDescription

constructGameDescription :: Board -> GameDescription
constructGameDescription board = GameDescription board (getGameState board) Nothing

handleAction :: GameDescription -> GameAction -> GameDescription
handleAction desc (M action) = case nextState of
    Nothing -> set lastError (Just IllegalMove) desc
    Just s -> constructGameDescription s
  where
    nextState = makeMove (_board desc)  action
handleAction _ (Restart) = constructGameDescription initialState

wireSignals :: Circuit era (DSignal era  GameAction) (SSignal era GameDescription)
wireSignals = arr $ SSignal.scan (constructGameDescription initialState) handleAction


ssMsg :: GameState -> String
ssMsg (Move p) = "Now moves: " ++ (show p)
ssMsg (Win p) = "Player "++ (show p) ++ " won"
ssMsg Draw = "Draw"

errMsg :: Maybe GameError -> String
errMsg Nothing = "\n"
errMsg (Just IllegalMove) = "\nIllegal move"

statusMessage :: GameDescription -> String
statusMessage gd = ssMsg (gd^.state) ++ errMsg (gd^.lastError)


