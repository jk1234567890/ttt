{-# LANGUAGE RankNTypes #-}
module TTT.AI where

import TTT.Game
import TTT.Signals
import FRP.Grapefruit.Circuit
import FRP.Grapefruit.Signal.Discrete as DSignal
import FRP.Grapefruit.Signal.Segmented as SSignal(SSignal, updates)
import FRP.Grapefruit.Signal.Continuous as CSignal
import FRP.Grapefruit.Signal

import Control.Arrow
import Control.Lens

import Helper.Async
import Helper.Delay

withAiCircuit :: AsyncWorkerProvider Board Action -> Symbol -> (Board -> IO Action) -> DelaySpace GameState -> forall era. Circuit era (DSignal era GameAction) (SSignal era GameDescription)
withAiCircuit awp player aiFun ds = (filterLoop . aiLoop wireSignals) aiFeedback
  where
    aiFeedback :: Circuit era (DSignal era GameDescription) (DSignal era Action)
    aiFeedback = arr (playerBoard (opponent player)) >>> (asyncWorkerCircuit $ awp aiFun)
    aiLoop :: 
      Circuit era (DSignal era GameAction) (SSignal era GameDescription) ->
      Circuit era (DSignal era GameDescription) (DSignal era Action) ->
      Circuit era (DSignal era GameAction) (SSignal era GameDescription)
    aiLoop gameCircuit aiCircuit = loop $ arr (uncurry DSignal.union) >>> gameCircuit >>> (arr id &&& (arr (SSignal.updates) >>> aiCircuit >>> arr (M <$>)))
    filterLoop :: Circuit era (DSignal era GameAction) (SSignal era GameDescription) -> Circuit era (DSignal era GameAction) (SSignal era GameDescription)
    filterLoop gameCircuit = loop $ filterActions2 player >>> gameCircuit >>> (arr id &&& (arr (fmap (^.state)) >>> delayCircuit ds ))

playerBoard :: Symbol -> (DSignal era GameDescription) -> (DSignal era Board)
playerBoard symbol = (DSignal.map (^.board)).(DSignal.filter ((==Just symbol).currentPlayer.(^.state)))

currentPlayer :: GameState -> Maybe Symbol
currentPlayer (Move p) = Just p
currentPlayer _ = Nothing

filterActions :: Symbol -> Circuit era (DSignal era  Action, DSignal era GameState) (DSignal era Action)
filterActions symbol = arr fun
  where
    fun :: (DSignal era Action, DSignal era GameState) -> (DSignal era Action)
    fun (ac, gs) = mapMaybe id (fst <$> DSignal.scan (Nothing, symbol == X) comb (union rac lfilter))
      where
        lfilter = (Left . iscp . currentPlayer) <$> gs
        rac = Right <$> ac
        iscp Nothing = False
        iscp (Just p) = p == symbol
        comb _ (Left False) = (Nothing, False)
        comb _ (Left True) = (Nothing, True)
        comb (_, False) (Right _) = (Nothing, False)
        comb (_, True) (Right a) = (Just a, True)

filterActions2 :: Symbol -> Circuit era (DSignal era GameAction, CSignal era GameState) (DSignal era GameAction)
filterActions2 symbol = arr fun
  where
    fun :: (DSignal era GameAction, CSignal era GameState) -> (DSignal era GameAction)
    fun (action, stat) = DSignal.mapMaybe id sampled
      where
        player = currentPlayer <$> stat
        transfun acti Nothing  = Just acti
        transfun acti (Just p)
          | p == symbol = Just acti
          | otherwise = Nothing
        sampled = (transfun <$> action) <#> player
