{-# LANGUAGE FlexibleContexts #-}
module TTT.AI.MCTS(mctsTTTAI) where

import Game.Interface
import Data.Either
import Data.List as L
import Data.Map.Strict as Map
import Data.Sequence as Seq
import Control.Monad.Random.Class
import Control.Monad.Random.Strict
import Control.Monad.Reader
import Control.Monad.Reader.Class

import TTT.Interface
import qualified TTT.Game as G

data MctsMove s m = MctsMove {
  visitCounter :: Int,
  qSum :: Double,
  node :: MctsNode s m
}

data MctsNode s m = MctsNode {
  state :: s,
  successors :: Map m (MctsMove s m)
}

ucb1 :: Double -> Int -> Int -> Double
ucb1 q ni sumn = q+(sqrt $ 2*(log (fromIntegral sumn))/(fromIntegral ni))

emptyNode :: s -> MctsNode s m
emptyNode = flip MctsNode Map.empty

getBestMove :: (Ord m) => MctsNode s m -> m
getBestMove = fst . maximumBy cmp . Map.toList . successors
  where
    cmp (_, MctsMove n1 q1 _) (_, MctsMove n2 q2 _) = compare (q1/fromIntegral n1) (q2/fromIntegral n2)

mcts :: (MonadRandom r, Ord m, MonadReader g r, GameRulesExtractor g s m p) => Int -> MctsNode s m -> r (MctsNode s m)
mcts 0 nn = return nn
mcts simulations nn = mctsIteration nn >>= mcts (simulations-1)

registerPayoff :: MctsMove s m -> Double -> MctsNode s m -> MctsMove s m
registerPayoff (MctsMove vc q _) u newNode = MctsMove (vc+1) (q+u) newNode

mctsIteration :: (MonadRandom r, Ord m, MonadReader g r, GameRulesExtractor g s m p) => MctsNode s m -> r (MctsNode s m)
mctsIteration node = fst <$> mctsSelection node

mctsSelection :: (Ord m, MonadRandom r, MonadReader g r, GameRulesExtractor g s m p) => MctsNode s m -> r (MctsNode s m, p->Double)
mctsSelection nn = do
  pp <- gamePayoffsOrPlayer st
  case pp of
    Left pFun -> return (nn, pFun)
    Right pl -> do
      pm <- gamePossibleMoves <*> pure st
      if (Prelude.length $ Map.toList (successors nn)) /= (Prelude.length $ pm)
        then mctsExpansion nn
        else do
           (newNode, pFun) <- mctsSelection (node $ successors nn ! selectedMove)
           return (MctsNode st (Map.insert selectedMove (registerPayoff selMN (pFun pl) newNode) succ), pFun)
  where
    st = state nn
    succ = successors nn
    nsum = L.foldl' (+) 0 (Prelude.map (visitCounter.snd) (Map.toList $ succ))
    (selectedMove, selMN) = maximumBy cmp (Map.toList succ)
    cmp (_, (MctsMove n1 q1 _)) (_, (MctsMove n2 q2 _)) = (ucb1 q1 n1 nsum) `compare` (ucb1 q2 n2 nsum)

mctsExpansion :: (Ord m, MonadRandom r, MonadReader g r, GameRulesExtractor g s m p) => MctsNode s m -> r (MctsNode s m, p->Double)
mctsExpansion node = do
  mm <- randomUnused
  nextState <- gameMakeMove <*> pure st <*> pure mm
  samplingResult <- mctsSimulation nextState
  let nn = emptyNode nextState
  pp <- gamePayoffsOrPlayer st
  let mmm = MctsMove 1 (samplingResult $ fromRight undefined pp) nn
  return (MctsNode st (Map.insert mm mmm ss), samplingResult)
  where
    randomUnused = do 
      um <- unusedMoves
      (um `index`) . (`mod`(Seq.length um))  <$> getRandom
    unusedMoves = Seq.filter (not.(`elem`(keys $ successors node))) <$> (gamePossibleMoves <*> pure st)
    st = state node
    ss = successors node

mctsSimulation :: (MonadRandom r, MonadReader g r, GameRulesExtractor g s m p) => s -> r (p->Double)
mctsSimulation state = (gamePayoffsOrPlayer state) >>= either
  return
  (const $ (randomSuccessor state >>= mctsSimulation))
  

randomSuccessor :: (MonadRandom r, MonadReader g r, GameRulesExtractor g s m p) => s -> r s
randomSuccessor state =  gameMakeMove <*> pure state <*> (randomMove state)

randomMove :: (MonadRandom r, MonadReader g r, GameRulesExtractor g s m p) => s -> r m
randomMove state = do
    moves <- gamePossibleMoves <*> pure state
    (moves `index`) . (`mod`(Seq.length moves))  <$> getRandom



mctsTTTAI :: G.Board -> (IO G.Action)
mctsTTTAI board = do
  (getBestMove . fst . (flip runReader tttGame) . (runRandT trainingResult)) <$> getStdGen
  where
    initialNode = MctsNode board Map.empty
    trainingResult = mcts 1000 initialNode

