module TTT.AI.Dumb(firstFromLeftAI) where

import Control.Concurrent
import Control.Applicative()

import TTT.Game

import Data.List

firstFromTopLeft :: Board -> Action
firstFromTopLeft board = maybe (error "No move possible") (uncurry Action) $ find ((==Nothing) . (uncurry (getField board))) coords 
  where
    coords = (,) <$>
      coordSeq <*>
      coordSeq

firstFromLeftAI :: Board -> IO Action
firstFromLeftAI board = threadDelay 2000000 >> (return $ firstFromTopLeft board)
