{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE Arrows #-}
{-# LANGUAGE BangPatterns #-}
module Main(main) where

import Control.Arrow
import Control.Lens
import Control.Monad
import Data.Maybe

import FRP.Grapefruit.Circuit
import FRP.Grapefruit.Signal
import FRP.Grapefruit.Signal.Discrete as DSignal
import FRP.Grapefruit.Signal.Segmented as SSignal

import TTT.AI
import TTT.AI.MCTS
import TTT.Game
import TTT.Signals

import Helper.Delay
import Helper.Async
import Helper.Board.Format
import Circuit.Console

data Command = Mv Action | Exit | BadInput | RRestart

isBadInput :: Command -> Bool
isBadInput BadInput = True
isBadInput _ = False

isExit :: Command -> Bool
isExit Exit = True
isExit _ = False

getAction :: Command -> Maybe GameAction
getAction (Mv a) = Just $ M a
getAction RRestart = Just Restart
getAction _ = Nothing

getNumField :: Board -> Coord -> Coord -> String
getNumField = curry $ curry $ (maybe "") show . (uncurry $ uncurry getField)

gameDescToMessage :: GameDescription -> String
gameDescToMessage desc =
  drawBoard getNumField (desc^.board) ++ "\n\n" ++ ssMsg (desc^.state) ++ "\n" ++ errMsg (desc^.lastError) ++ "\n"

parseInput :: String -> Command
parseInput "exit" = Exit
parseInput "restart" = RRestart
parseInput str = fromMaybe BadInput $ Mv <$> parseMove Action str

wiredApplication ::
  (ScheduleAction -> AsyncWorkerProvider Board Action) ->
  DelaySpace GameState -> ScheduleAction ->  Circuit era (Inputs era) (Outputs era)
wiredApplication awp ds sched = proc (Inputs appSt lineIn) -> do
   let input = parseInput <$> lineIn
   desc <- (withAiCircuit (awp sched) TTT.Game.X mctsTTTAI ds) -< moveSignal input
   let gameStateOut = gameDescToMessage <$> union (updates desc) (appSt #> desc)
   let badInputOut = const "Bad input" <$> badMoveSignal input
   returnA -< Outputs (gameStateOut `union` badInputOut) (exitSignal input)
   where
    moveSignal = DSignal.mapMaybe getAction
    exitSignal = DSignal.mapMaybe (guard . isExit)
    badMoveSignal = DSignal.mapMaybe (guard . isBadInput)


main :: IO ()
main = do
  awp <- prepareAsyncIOWorker
  ds <- prepareDelayCircuit
  runConsoleApp $ wiredApplication awp ds
