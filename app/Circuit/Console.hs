{-# LANGUAGE RankNTypes #-}
module Circuit.Console(
  runConsoleApp, Inputs(..), Outputs(..), ScheduleAction
    )where

import Control.Arrow
import Control.Concurrent
import Control.Monad
import Data.IORef

import FRP.Grapefruit.Circuit
import FRP.Grapefruit.Setup
import FRP.Grapefruit.Signal
import FRP.Grapefruit.Signal.Discrete as DSignal

type ScheduleAction = IO () -> IO ()

repeatUntil :: IORef Bool -> IO () -> IO ()
repeatUntil cond action = (readIORef cond)  >>= flip unless (action >> repeatUntil cond action)

data Inputs era = Inputs {
  appStarted :: DSignal era (),
  lineInput :: DSignal era String
}

data Outputs era =  Outputs {
  lineOutput :: DSignal era String,
  endApp :: DSignal era ()
}

consoleLineInput :: ScheduleAction -> forall era. Circuit era () (DSignal era String)
consoleLineInput sched = produce $ DSignal.producer setupHandler
  where
    setupHandler handler = setup $
      ((forkIO $ forever (getLine >>= sched  . handler)) >>= return .  killThread)

startedEvent :: ScheduleAction -> forall era. Circuit era () (DSignal era ())
startedEvent sched = produce $ DSignal.producer (setup . (>>= return . return) . (sched <$> ($())))

runConsoleApp :: (ScheduleAction -> (forall era. Circuit era (Inputs era) (Outputs era))) -> IO ()
runConsoleApp circuit = do
  eventChan <- newChan
  let schedule = writeChan eventChan
  endAppRef <- newIORef False
  let
    endgameConsumer = DSignal.consumer $ const $ writeIORef endAppRef True
    lineConsumer = DSignal.consumer putStrLn
    wiredCircuit = (startedEvent schedule &&& consoleLineInput schedule)
      >>> arr (uncurry Inputs)
      >>> circuit schedule
      >>> (arr lineOutput &&& arr endApp)
      >>> (consume lineConsumer *** consume endgameConsumer)
      >>> arr (const ())
    in do
      ((), fin) <- create wiredCircuit ()
      repeatUntil endAppRef $ join $  readChan eventChan
      fin
    

