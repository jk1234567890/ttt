{-# LANGUAGE ExistentialQuantification, RankNTypes #-}
{-# LANGUAGE Arrows #-}
module Main(main) where

import TTT.Game
import TTT.Signals
import TTT.AI
import TTT.AI.MCTS
import Helper.Async
import Helper.Delay

import FRP.Grapefruit.Circuit(Circuit)
import FRP.Grapefruit.Signal.Discrete as DSignal
import FRP.Grapefruit.Signal.Segmented as SSignal
import Graphics.UI.Grapefruit.Circuit
import Graphics.UI.Grapefruit.Backend.Basic as BB
import Graphics.UI.Grapefruit.Item
import Graphics.UI.Grapefruit.GTK
import Data.Record as R
import Control.Lens
import Control.Arrow
import Data.List(foldl')

import Graphics.UI.Gtk.General.General(postGUIAsync)

main :: IO ()
main = do
  awp <- prepareAsyncIOWorker <*> pure postGUIAsync
  ds <- prepareDelayCircuit
  run GTK (mainWindow $ withAiCircuit awp TTT.Game.X mctsTTTAI ds) ()

getCellVal :: Coord -> Coord -> GameDescription -> String
getCellVal r c desc =
  case getField (desc^.board) r c of
    Just TTT.Game.X -> "X"
    Just O -> "O"
    Nothing -> ""

boardUI :: (BasicUIBackend uiBackend) => UICircuit Widget uiBackend era (SSignal era GameDescription) (DSignal era GameAction)
boardUI = proc gameDesc -> do
  R.X `With` sig <- BB.box Vertical `with` boardUIInner -< R.X `With` gameDesc
  returnA -< M <$> sig

boardUIInner :: (BasicUIBackend uiBackend) => UICircuit Widget uiBackend era (SSignal era GameDescription) (DSignal era Action)
boardUIInner = (sequenceArr $ fmap boardRow coordSeq) >>> (arr $ foldl' DSignal.union DSignal.empty)

boardRowInner :: (BasicUIBackend uiBackend) => Coord -> UICircuit Widget uiBackend era (SSignal era GameDescription) (DSignal era Action)
boardRowInner r = (sequenceArr $ fmap (boardCell r) coordSeq) >>> (arr $ foldl' DSignal.union DSignal.empty)

boardRow :: (BasicUIBackend uiBackend) => Coord -> UICircuit Widget uiBackend era (SSignal era GameDescription) (DSignal era Action)
boardRow r = proc gameDesc -> do
  R.X `With` sig <- BB.box Horizontal `with` boardRowInner r -< R.X `With` gameDesc
  returnA -< sig

boardCell :: (BasicUIBackend uiBackend) => Coord -> Coord -> UICircuit Widget uiBackend era (SSignal era GameDescription) (DSignal era Action)
boardCell r c = proc gameDesc -> do
  R.X :& Push := push <- just pushButton -< R.X :& Text := getCellVal r c <$> gameDesc
  returnA -< DSignal.map (const $ Action r c) push

statusBar :: (BasicUIBackend uiBackend) => UICircuit Widget uiBackend era (SSignal era GameDescription) ()
statusBar = proc gameDesc -> do
  R.X <- just label -< R.X :& Text := statusMessage <$> gameDesc
  returnA -< ()

restartButton :: (BasicUIBackend uiBackend) => UICircuit Widget uiBackend era a (DSignal era GameAction)
restartButton =proc _ -> do
  R.X :& Push := push <- just pushButton -< R.X :& Text := pure "Restart game"
  returnA -< DSignal.map (const Restart) push

mainWindow :: (BasicUIBackend uiBackend) => Circuit era (DSignal era GameAction) (SSignal era GameDescription) -> UICircuit Window uiBackend era () (DSignal era ())
mainWindow gameCircuit = proc () -> do
  rec let
      gameDesc <- fromCircuit gameCircuit -< union signal restart
      R.X :& Closure := closure`With` R.X `With` (signal, (_, restart)) <- window `with` BB.box Vertical `with` (boardUI &&& statusBar &&& restartButton) -< R.X :& Title := pure "Tic Tac Toe" `With` R.X `With` gameDesc
  returnA -< closure



sequenceArr :: (Arrow a) => [a b c] -> a b [c]
sequenceArr [] = arr (const [])
sequenceArr (x:xs) = x &&& (sequenceArr xs) >>> arr (uncurry (:))
